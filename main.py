from flask import Flask
from flask_restful import Api

from views import *

application = Flask('application')
api = Api(application)

# Routes registration
api.add_resource(HealthCheck, '/health-check')
api.add_resource(JobSearch, '/job-search/<string:keywords>/<string:location>/<int:page>')
