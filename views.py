from flask_restful import Resource

from external import JoobleAPI

jbapi = JoobleAPI()


class HealthCheck(Resource):
    def get(self):
        return {'status': 'ok'}


class JobSearch(Resource):
    def get(self, keywords, location, page=1):
        response = jbapi.simple_search(keywords=keywords, location=location, page=page)
        return response
