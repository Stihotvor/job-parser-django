from requests import request


class JoobleAPI:
    def __init__(self):
        self.host = "pl.jooble.org"
        self.key = "b4defc54-9c12-44ad-b043-e0da44772c8f"
        self.headers = {"Content-type": "application/x-www-form-urlencoded"}

    def simple_search(self, keywords, location, page):
        data = {"keywords": keywords,
                "location": location,
                "page": page}
        respone = request("POST", f"https://{self.host}/api/{self.key}", json=data)
        print(respone.json())
        return respone.json()

# headers = {"Content-type": "application/x-www-form-urlencoded"}
# connection.request('POST','/api/' + key, body, headers)
# response = connection.getresponse()
# print(response.status, response.reason)
# data = json.loads(response.read().decode("utf-8"))
# print("Jobs found: %s" % data["totalCount"])
# print("----------------------------------------\n")
#
# for job in data["jobs"]:
#     print("Id: %s" % job["id"])
#     print("Title: %s" % job["title"])
#     print("Location: %s" % job["location"])
#     print("Company: %s" % job["company"])
#     print("Salary: %s" % job["salary"])
#     print("Location: %s" % job["location"])
#     print("Snippet: %s" % job["snippet"])
#     print("----------------------------------------")
#     print("Updated: %s" % job["updated"])
#     print("Source: %s" % job["source"])
#     print("Link: %s" % job["link"])
#     print("----------------------------------------")
#     print("----------------------------------------")