from main import application


if __name__ == '__main__':
    application.run(debug=False, port=8000)
